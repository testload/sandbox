# Sandbox

**Working copy example of loadtest project:**

- **Scripts** - [JMeter](https://gitlab.com/testload/jmeter) scripts (JMX) for [Google's main page](https://www.google.com) "loadtesting" (just for example)

- [**Profiles & Results**](https://gitlab.com/testload/sandbox/wikis/home)